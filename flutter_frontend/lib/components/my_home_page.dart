import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_frontend/components/add_modify_car.dart';
import 'package:flutter_frontend/components/car_card.dart';
import 'package:flutter_frontend/models/car.dart';
import 'package:flutter_frontend/services/car_service.dart';

class MyHomePage extends StatefulWidget {
  static String routeName = "Home";
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CarService? carService;
  bool _isLoading = false;

  List<Car> carsList = [];
  Timer? _timer;

  @override
  void initState() {
    CarService.getData();
    _timer = Timer.periodic(
        const Duration(milliseconds: 100), (timer) => CarService.getData());
    super.initState();
  }

  @override
  void dispose() {
    if (_timer!.isActive) _timer!.cancel();

    CarService.streamController.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Cars"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const AddModifyCar(
                        carId: -1,
                      )));
        },
        child: const Icon(Icons.add),
      ),
      body: StreamBuilder(
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            carsList = snapshot.data;
            return GridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 5,
                mainAxisSpacing: 10,
                padding: const EdgeInsets.all(8),
                shrinkWrap: true,
                children: List.generate(
                    carsList.length,
                    (index) => CarCard(
                        carId: carsList[index].carId.toString(),
                        make: carsList[index].make,
                        model: carsList[index].model,
                        image: carsList[index].image,
                        year: carsList[index].year.toString())));
          }

          return const Center(child: CircularProgressIndicator());
        },
        stream: CarService.streamController.stream,
      ),
    );
  }
}
