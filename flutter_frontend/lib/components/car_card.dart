import 'package:flutter/material.dart';
import 'package:flutter_frontend/components/add_modify_car.dart';
import 'package:flutter_frontend/services/car_service.dart';

class CarCard extends StatefulWidget {
  final String carId;
  final String make;
  final String model;
  final String image;
  final String year;

  const CarCard(
      {Key? key,
      required this.carId,
      required this.make,
      required this.model,
      required this.image,
      required this.year})
      : super(key: key);

  @override
  _CarCardState createState() => _CarCardState();
}

class _CarCardState extends State<CarCard> {
  CarService? carService;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          GestureDetector(
            onLongPress: () {
              _showCarsInfos(context);
            },
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => AddModifyCar(
                      carId: int.parse(widget.carId),
                      image: widget.image,
                      year: int.parse(widget.year),
                      model: widget.model,
                      make: widget.make)));
            },
            child: Card(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 200,
                    height: 100,
                    child: Image.network(
                      CarService.API + 'static/uploads/' + widget.image,
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(
                    height: 50,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Row(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Column(
                              children: [
                                Text(
                                  widget.make,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(widget.model)
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            ),
                          ),
                          Align(
                              alignment: Alignment.center,
                              child: Text(
                                widget.year.toString(),
                                style: const TextStyle(
                                    fontSize: 30, fontFamily: 'Roboto'),
                              )),
                        ],
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showCarsInfos(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SizedBox(
              height: MediaQuery.of(context).size.height * .5,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .25,
                      child: Image.network(
                        CarService.API + 'static/uploads/' + widget.image,
                        fit: BoxFit.cover,
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * .15,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  widget.year,
                                  style: const TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(width: 10),
                                Text(
                                  widget.make,
                                  style: const TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  widget.year,
                                  style: TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white.withOpacity(0)),
                                ),
                                const SizedBox(width: 10),
                                Text(widget.model,
                                    style: const TextStyle(fontSize: 32)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}
