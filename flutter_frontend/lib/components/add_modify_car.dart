import 'dart:convert';
import 'dart:io';
import 'package:flutter_frontend/models/brand.dart';
import 'package:flutter_frontend/models/car.dart';
import 'package:flutter_frontend/services/car_service.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class AddModifyCar extends StatefulWidget {
  final int carId;
  final String make;
  final String model;
  final String image;
  final int year;

  bool get isNew => carId == -1;
  const AddModifyCar(
      {Key? key,
      this.carId = -1,
      this.make = "",
      this.model = "",
      this.image = "",
      this.year = 0})
      : super(key: key);
  @override
  _AddModifyCarState createState() => _AddModifyCarState();
}

class _AddModifyCarState extends State<AddModifyCar> {
  CarService? carService;
  File? image;
  Future pickImageFromGallery() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;

      final imageTmp = File(image.path);
      setState(() => this.image = imageTmp);
    } on PlatformException catch (e) {
      print('Could not pick image : $e');
    }
  }

  Future pickImageFromCamera() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.camera);
      if (image == null) return;

      final imageTmp = File(image.path);
      setState(() => this.image = imageTmp);
    } on PlatformException catch (e) {
      print('Could not pick image : $e');
    }
  }

  // Dropdown button part

  final String URL = "https://the-vehicles-api.herokuapp.com/";
  List<Brand> data = [];

  Future getAllMakers() async {
    var res = await http.get(Uri.parse(URL + 'brands'),
        headers: {"Accept": "application/json"});
    List<dynamic> resBody = json.decode(res.body);

    for (var item in resBody) {
      data.add(Brand.fromMap(item));
    }
    setState(() {});
    Future.delayed(const Duration(seconds: 1));
  }

  @override
  void initState() {
    super.initState();
    getAllMakers();
    if (widget.isNew == false) {
      modelController = TextEditingController(text: widget.model);
      yearController = TextEditingController(text: widget.year.toString());
      _mySelection = data.isNotEmpty
          ? _mySelection = data.elementAt(0).brand
          : widget.make;
    } else {
      _mySelection =
          data.isNotEmpty ? _mySelection = data.elementAt(0).brand : 'Acura';
      modelController = TextEditingController();
      yearController = TextEditingController();
    }
  }

  final _formKey = GlobalKey<FormState>();
  String getImageName(String image) {
    return image != ''
        ? image.substring(image.lastIndexOf('/') + 1, image.length)
        : 'placeholder.png';
  }

  String _mySelection = 'Acura';
  TextEditingController? modelController;
  TextEditingController? yearController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.isNew ? "Add a New Car" : "Modify Car's Infos"),
          actions: widget.isNew
              ? []
              : [
                  Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () async {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: const Text("Warning"),
                                  content: const Text(
                                      "Are you sure you want to remove this car?"),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          CarService.deleteCar(
                                              widget.carId.toString());
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text("Yes")),
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop(false);
                                        },
                                        child: const Text("No")),
                                  ],
                                );
                              });
                        },
                        child: const Icon(
                          Icons.delete_forever_outlined,
                          size: 26.0,
                        ),
                      )),
                ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
              key: _formKey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          widget.isNew
                              ? image != null
                                  ? SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .9,
                                      child: Image.file(image!,
                                          width: 500,
                                          height: 200,
                                          fit: BoxFit.cover))
                                  : const Image(
                                      image:
                                          AssetImage('assets/placeholder.png'),
                                      width: 200,
                                      height: 200,
                                      fit: BoxFit.cover,
                                    )
                              : image == null
                                  ? SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .9,
                                      child: Image.network(
                                          CarService.API +
                                              'static/uploads/' +
                                              widget.image,
                                          width: 500,
                                          height: 200,
                                          fit: BoxFit.cover))
                                  : SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          .9,
                                      child: Image.file(image!,
                                          width: 500,
                                          height: 200,
                                          fit: BoxFit.cover)),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                DropdownButton(
                                  isExpanded: true,
                                  value: _mySelection,
                                  items: List.generate(data.length, (index) {
                                    return DropdownMenuItem<String>(
                                      child: Text(data.elementAt(index).brand),
                                      value: data.elementAt(index).brand,
                                    );
                                  }).toList(),
                                  onChanged: (String? newVal) {
                                    setState(() {
                                      _mySelection = newVal!;
                                    });
                                  },
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                TextFormField(
                                  decoration: const InputDecoration(
                                      hintText: "Model name"),
                                  controller: modelController,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return "Please enter a car model";
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                TextFormField(
                                  keyboardType: TextInputType.number,
                                  controller: yearController,
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return "Please enter a production year";
                                    }
                                    if (int.parse(value!) >
                                            DateTime.now().year + 1 ||
                                        int.parse(value) < 1885) {
                                      return "Please enter a valid year";
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                    hintText: "Year made", /*errorText: error*/
                                  ),
                                ),
                                const SizedBox(
                                  height: 50,
                                ),
                                Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      customButton(
                                          title: 'Pick From Gallery',
                                          icon: Icons.photo_library_outlined,
                                          onClicked: () =>
                                              pickImageFromGallery()),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      const Center(
                                          child: Text(
                                        "or",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(color: Colors.grey),
                                      )),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      customButton(
                                          title: 'Capture Image',
                                          icon: Icons.photo_camera_outlined,
                                          onClicked: () =>
                                              {pickImageFromCamera()}),
                                      const SizedBox(
                                        height: 50,
                                      ),
                                      customButton(
                                          title: 'Submit',
                                          icon: Icons.done,
                                          onClicked: () {
                                            if (_formKey.currentState!
                                                .validate()) {
                                              widget.isNew
                                                  ? CarService.createCar(
                                                      Car(
                                                          carId: -1,
                                                          make:
                                                              _mySelection, //Controller name.text
                                                          model:
                                                              modelController!
                                                                  .text,
                                                          image: getImageName(
                                                              image!.path),
                                                          year: int.parse(
                                                              yearController!
                                                                  .text),
                                                          postedAt:
                                                              DateTime.now()),
                                                      image!.path)
                                                  : CarService.updateCar(
                                                      Car(
                                                          carId: widget.carId,
                                                          make:
                                                              _mySelection, //Controller name.text
                                                          model:
                                                              modelController!
                                                                  .text,
                                                          image: getImageName(
                                                              image != null
                                                                  ? image!.path
                                                                  : ""),
                                                          year: int.parse(
                                                              yearController!
                                                                  .text),
                                                          postedAt:
                                                              DateTime.now()),
                                                      image != null
                                                          ? image!.path
                                                          : "");
                                              Navigator.of(context).pop(true);
                                            }
                                          })
                                    ])
                              ])
                        ])
                  ])),
        ));
  }

  Widget customButton(
      {required String title,
      required IconData icon,
      required VoidCallback onClicked}) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          minimumSize: const Size.fromHeight(56),
          primary: Colors.blue,
          onPrimary: Colors.white,
          textStyle: const TextStyle(fontSize: 20)),
      onPressed: onClicked,
      child: Row(
        children: [
          Icon(icon, size: 28),
          const SizedBox(width: 20),
          Text(title)
        ],
      ),
    );
  }
}
