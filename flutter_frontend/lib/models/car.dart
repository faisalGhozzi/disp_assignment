import 'dart:convert';

class Car {
  final int carId;
  final String make;
  final String model;
  final String image;
  final int year;
  final DateTime postedAt;

  Car(
      {required this.carId,
      required this.make,
      required this.model,
      required this.image,
      required this.year,
      required this.postedAt});

  Map<String, dynamic> toMap() {
    return {
      'carId': carId,
      'make': make,
      'model': model,
      'image': image,
      'year': year,
      'postedAt': postedAt.millisecondsSinceEpoch,
    };
  }

  factory Car.fromMap(Map<String, dynamic> map) {
    return Car(
      carId: map['car_id']?.toInt() ?? 0,
      make: map['make'] ?? '',
      model: map['model'] ?? '',
      image: map['image'] ?? '',
      year: map['year']?.toInt() ?? 0,
      postedAt: DateTime.parse(map['timestamp']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Car.fromJson(String source) => Car.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Car(carId: $carId, make: $make, model: $model, image: $image, year: $year, postedAt: $postedAt)';
  }
}
