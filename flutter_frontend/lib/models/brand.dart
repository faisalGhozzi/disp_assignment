import 'dart:convert';

class Brand {
  final int id;
  final String brand;

  Brand({required this.id, required this.brand});

  factory Brand.fromMap(Map<String, dynamic> map) {
    return Brand(id: map['id'] ?? 0, brand: map['brand'] ?? '');
  }

  factory Brand.fromJson(String source) => Brand.fromMap(json.decode(source));
}
