import 'dart:async';
import 'dart:convert';
import 'package:flutter_frontend/models/car.dart';
import 'package:http/http.dart' as http;

class CarService {
  static const API = 'http://192.168.1.7:5000/';
  static StreamController streamController = StreamController();

  static Future getData() async {
    List<Car> carsList = [];
    http.Response response = await http.get(Uri.parse(API + "api/cars"));

    List<dynamic> carsFromServer = json.decode(response.body);
    if (carsFromServer.isNotEmpty) {
      for (var item in carsFromServer) {
        carsList.add(Car.fromMap(item));
      }
    }
    streamController.add(carsList);
  }

  static Future<Car> getCar(int carId) async {
    http.Response response =
        await http.get(Uri.parse(API + "api/cars/" + carId.toString()));

    Car car = json.decode(response.body);
    return car;
  }

  static Future<bool> createCar(Car item, String imagePath) async {
    var request = http.MultipartRequest(
      "POST",
      Uri.parse(API + 'api/cars'),
    );
    request.fields["make"] = item.make;
    request.fields["model"] = item.model;
    request.fields["year"] = item.year.toString();
    http.MultipartFile multipartFile =
        await http.MultipartFile.fromPath('image', imagePath);
    request.fields["image"] = multipartFile.filename!;
    request.files.add(multipartFile);
    await request.send().then((data) {
      if (data.statusCode == 201) {
        return true;
      } else {
        return false;
      }
    });
    return false;
  }

  static Future<bool> updateCar(Car item, String imagePath) async {
    var request = http.MultipartRequest(
      "PUT",
      Uri.parse(API + 'api/cars/' + item.carId.toString()),
    );
    request.fields["make"] = item.make;
    request.fields["model"] = item.model;
    request.fields["year"] = item.year.toString();
    if (imagePath != '') {
      http.MultipartFile multipartFile =
          await http.MultipartFile.fromPath('image', imagePath);
      request.fields["image"] = multipartFile.filename!;
      request.files.add(multipartFile);
    } else {
      request.fields["image"] = item.image;
    }
    await request.send().then((data) {
      if (data.statusCode == 201) {
        return true;
      } else {
        return false;
      }
    });
    return false;
  }

  // static Future<bool> updateCar(Car item) {
  //   return http
  //       .put(Uri.parse(API + 'api/cars/' + item.carId.toString()),
  //           headers: {'Content-type': 'application/json'},
  //           body: jsonEncode({
  //             'make': item.make,
  //             'model': item.model,
  //             'year': item.year,
  //             'image': item.image,
  //           }))
  //       .then((data) {
  //     if (data.statusCode == 201) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   });
  // }

  static Future<bool> deleteCar(String carID) {
    return http.delete(Uri.parse(API + 'api/cars/' + carID)).then((data) {
      if (data.statusCode == 204) {
        return true;
      }
      return false;
    });
  }
}
