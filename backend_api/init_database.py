import os
from config import db
from model.car import Car

CARS = [
    {"make": "Ferrari", "model": "Testarossa", "year": 1984, "image": "ferrari_testarossa_1984.jpeg"},
    {"make": "Lamborghini", "model": "Miura", "year": 1966, "image": "lamborghini_miura_1966.jpg"},
    {"make": "Bugatti", "model": "EB110", "year": 1991, "image": "Bugatti_EB110_1991.jpg"},
    {"make": "Porsche", "model": "992", "year": 2019, "image": "porsche_911_992_carrera_s_2019.jpg"},
    {"make": "Nissan", "model": "Z", "year": 2023, "image": "Nissan_Z_2023.jpg"},
]

if os.path.exists("cars.db"):
    os.remove("cars.db")

db.create_all()

for car in CARS:
    c = Car(make=car.get("make"), model=car.get("model"), year=car.get("year"), image=car.get("image"))
    db.session.add(c)

db.session.commit()
