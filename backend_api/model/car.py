from datetime import datetime
import this

from time import time
from config import db, ma



class Car(db.Model):
    __tablename__ = "car"
    car_id = db.Column(db.Integer, primary_key=True)
    make = db.Column(db.String(32))
    model = db.Column(db.String(32))
    year = db.Column(db.Integer, nullable=False)
    image = db.Column(db.String(100))
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow
    )
    
    
    def __init__(self,  make, model, year, image) -> None:
        super().__init__()
        self.make = make
        self.model = model
        self.year = year
        self.image = image


class CarSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Car
        sqla_session = db.session
        load_instance = True
