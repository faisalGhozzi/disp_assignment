from datetime import datetime
from fileinput import filename
from flask import make_response, abort, request
from config import db
from model.car import Car, CarSchema
import werkzeug
from werkzeug.datastructures import ImmutableMultiDict
import os
import json

import sys
from json import JSONEncoder

import logging
logging.basicConfig(level=logging.DEBUG)

class CarEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__


def read_all():
    cars = Car.query.order_by(Car.make).all()

    car_schema = CarSchema(many=True)
    data = car_schema.dump(cars)
    return data


def read_one(car_id):
    car = Car.query.filter(Car.car_id == car_id).one_or_none()

    if car is not None:
        car_schema = CarSchema()
        data = car_schema.dump(car)
        return data
    abort(404,"Not cars found")


def create():
    make = request.form.get('make')
    model = request.form.get('model')
    year = request.form.get('year')
    
    existing_car = (
        Car.query.filter(Car.make == make)
        .filter(Car.model == model).filter(Car.year == year)
        .one_or_none()
    )

    if existing_car is None:

        image_file = request.files['image']
        filename = werkzeug.utils.secure_filename(image_file.filename) # or imageFile.name
        image_file.save('./static/uploads/'+ filename)
                
        schema = CarSchema()
        
        new_car = schema.load({
                "make" : make,
                "model": model,
                "year" : year,
                "image": filename,
            }, session=db.session)

        db.session.add(new_car)
        db.session.commit()

        data = schema.dump(new_car)

        return data, 201
    else:
        abort(
            409,
            "Car already in garage"
        )

def update(car_id, car):
    
    update_car = Car.query.filter(
        Car.car_id == car_id
    )
    
    make = request.form.get('make')
    model = request.form.get('model')
    year = request.form.get('year')
    filename = request.form.get('image')
    
    
    
    
    existing_car = (
        Car.query.filter(Car.make == make)
        .filter(Car.model == model).filter(Car.year == year)
        .one_or_none()
    )

    if update_car is None:
        abort(
            404,
            "Car does not exist in garage"
        )

    elif (
        existing_car is not None and existing_car.car_id != car_id
    ):
        abort(
            409,
            "Car is in garage"
        )

    else :

        if 'image' in request.files:
            image_file = request.files['image']
            filename = werkzeug.utils.secure_filename(image_file.filename) # or imageFile.name
            image_file.save('./static/uploads/'+ filename)  
            
                
        schema = CarSchema()
        
        update = schema.load({
                "car_id": car_id,
                "make" : make,
                "model": model,
                "year" : year,
                "image": filename,
            }, session=db.session)

        db.session.merge(update)
        db.session.commit()

        data = schema.dump(update_car)

        return data, 200


def delete(car_id):

    car = Car.query.filter(Car.car_id == car_id).one_or_none()
    
    if car is not None:
        db.session.delete(car)
        db.session.commit()
        return make_response(
            "Car Removed", 200
        )

    else:
        abort(
            404,
            "Car does not exist"
        )
