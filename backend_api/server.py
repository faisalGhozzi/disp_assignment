from flask import render_template
import os;
import config

connex_app = config.connex_app

connex_app.add_api("swagger.yml")

@connex_app.route("/")
def home():
    return "<center><h1>Please use the mobile application :). thank you!</h1><center>"


if __name__ == "__main__":
    # os.system('python init_database.py')
    connex_app.run(host="0.0.0.0", port=5000, debug=True)
